package com.patrickramosruas.msemailsent.enums;

public enum StatusEmail {
    SENT,
    ERROR;
}
