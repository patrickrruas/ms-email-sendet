package com.patrickramosruas.msemailsent.repositories;

import com.patrickramosruas.msemailsent.models.EmailModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface EmailRepository extends JpaRepository<EmailModel, UUID> {
}
