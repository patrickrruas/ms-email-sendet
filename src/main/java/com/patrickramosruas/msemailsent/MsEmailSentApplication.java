package com.patrickramosruas.msemailsent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsEmailSentApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsEmailSentApplication.class, args);
	}

}
